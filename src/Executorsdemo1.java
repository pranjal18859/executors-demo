import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Executorsdemo1 {
    public static void main(String[] args) {
        ExecutorService executorService= Executors.newSingleThreadExecutor();
        for(int i=0;i<10;i++){
            Runnable worker=new WorkerThread(" "+i);
            executorService.execute(worker);
        }
        executorService.shutdown();
        while (!executorService.isTerminated()){

        }
        System.out.println("All tasks are finished");

    }
}

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ForkJoinWorkerThread;

public class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");
        ExecutorService executorService= Executors.newFixedThreadPool(5);
        for(int i=0;i<10;i++){
            Runnable worker=new WorkerThread(" "+i);
            executorService.execute(worker);
        }
        executorService.shutdown();
        while (!executorService.isTerminated())
        {

        }
        System.out.println("All tasks are finished");
    }
}